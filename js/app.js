window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
    if(e.matches == true) {
        document.querySelector('body').classList.add('dark')
    }
});

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('../sw.js').then( () => {
        console.log('Service Worker Registered')
        })
    })
}