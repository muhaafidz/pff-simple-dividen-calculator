# CHANGELOG

## v1.3.1
- Add maskable icon (Temp)
## v1.3.0
- Add PWA Support

## v1.2.0
- Add Initial Saving Input

## v1.1.0
- Add Graph Projection

## v1.0.0
- Basic dividen input
- Tabular output